# Demo

Er zit in dit projectje een springboot restcontroller. Na het clonen van de git, of als je de zip hebt gedownload en uitgepakt, kan je controleren of het projectje draait. Dit kan je doen met:

~~~
.\gradlew.bat bootrun 
~~~ 

In de browser kan je vervolgens de [http://localhost:8080/hello](http://localhost:8080/hello) opnemen. Deze zal dan "Hello World :o)" moeten geven. 

Het toont vervolgens aan dat gradlew kan worden gebruikt en dat java is ingestalleerd. 

je mag ook het project in een IDE stoppen, als deze ook ondersteuning bied aan gradle. We gebruiken [Eclipse](https://www.eclipse.org/downloads/) maar een andere mag ook. 
